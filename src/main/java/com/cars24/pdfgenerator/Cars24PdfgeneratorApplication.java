package com.cars24.pdfgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cars24PdfgeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(Cars24PdfgeneratorApplication.class, args);
	}

}
